<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estate_licenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordinal_number')->unsigned();
            $table->string('state')->nullable();
            $table->string('license_company')->nullable();
            $table->string('license_number')->nullable();
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')
                ->references('id')->on('profiles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estate_licenses');
    }
}
