-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2018 at 10:13 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebroker`
--

-- --------------------------------------------------------

--
-- Table structure for table `estate_licenses`
--

CREATE TABLE `estate_licenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordinal_number` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `estate_licenses`
--

INSERT INTO `estate_licenses` (`id`, `ordinal_number`, `state`, `license_company`, `license_number`, `profile_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Alabama', 'primary license', '111', 4, '2018-10-03 18:13:04', '2018-10-03 18:13:04'),
(2, 2, 'Delaware', 'additional license', '222', 4, '2018-10-03 18:13:04', '2018-10-03 18:13:04'),
(3, 3, 'Iowa', 'added license', '333', 4, '2018-10-03 18:13:04', '2018-10-03 18:13:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estate_licenses`
--
ALTER TABLE `estate_licenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `estate_licenses_profile_id_foreign` (`profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `estate_licenses`
--
ALTER TABLE `estate_licenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `estate_licenses`
--
ALTER TABLE `estate_licenses`
  ADD CONSTRAINT `estate_licenses_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
