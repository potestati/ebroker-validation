<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role =  App\Role::create([
            'name' => 'admin'
        ]);
        $role =  App\Role::create([
            'name' => 'supervisor'
        ]);
        $role =  App\Role::create([
            'name' => 'user'
        ]);

    }
}
