<?php

use Illuminate\Database\Seeder;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'admin')->first();
        $role_supervisor  = Role::where('name', 'supervisor')->first();
        $role_user  = Role::where('name', 'user')->first();


        $user =  App\User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);
        $user->roles()->attach($role_admin);

        $user =  App\User::create([
            'name' => 'supervisor',
            'email' => 'supervisor@admin.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);
        $user->roles()->attach($role_supervisor);
        $user =  App\User::create([
            'name' => 'agent',
            'email' => 'agent@admin.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);
        $user->roles()->attach($role_user);

    }
}
