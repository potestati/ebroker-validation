<?php

use Illuminate\Database\Seeder;
use App\User;
class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $profile =  App\Profile::create([
            'first_name' => 'Admin',
            'last_name' => 'Adminic',
            'city' => 'Novi Sad',
            'user_id' => 1
        ]);
        $profile =  App\Profile::create([
            'first_name' => 'Supervisor',
            'last_name' => 'Supervisoric',
            'city' => 'Belgrade',
            'user_id' => 2
        ]);
        $profile =  App\Profile::create([
            'first_name' => 'Agent',
            'last_name' => 'Agentic',
            'city' => 'Nis',
            'user_id' => 3
        ]);

    }
}
