<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstateLicense extends Model
{
    protected $fillable = [

        'ordinal_number',
        'state',
        'license_company',
        'license_number',
        'additional_state',
        'additionalcomp_license',
        'additionallicense_number',
        'profile_id',
    ];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
}
