<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name'
    ];

    public function users()
    {
        //one user can have more than one roles
        return $this->belongsToMany(User::class);
    }

}
