<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Profile extends Model
{
    protected $fillable = [
        'verified',
        'supervising_broker',
        'supervising_broker_name',
        'supervising_broker_email',
        'first_name',
        'last_name',
        'address',
        'city',
        'state',
        'zip_code',
        'phone_number',
        'company_website',
        'logo',
        'terms_of_services',
        'license',
        'user_id',
    ];

    
    public function user() {
        return $this->belongsTo('App\User'); //,'user_id'
    }
    
    public static function has(string $string, string $string1, int $int)
    {
    }

    public function registeredBroker()
    {
        // vesano sa db one to one , meny to one, one to meny and meny to meny
        //ovde pozivamo model User u model Profile jer ako user odabere odredjenu licencu za brokera koju poseduje to je , relacija u db na bazu tabela users
        //pa onda pozivamo tu funkciju ovu tacnije funkciju registeredBroker() u controller i podatke iz User u Profile kontroller
        return $this->hasMany('App\User');
    }

    public function estateLicense()
    {
        return $this->hasMany('App\EstateLicense');
    }



}
