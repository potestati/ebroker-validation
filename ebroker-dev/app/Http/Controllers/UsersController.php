<?php

namespace App\Http\Controllers;

use App\Mail\VerifyMail;
use App\User;
use App\Profile;
use App\Role;
use App\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    public function __construct(){

        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with('users', User::all());
    }

    public function unassigned()
    {
        return view('admin.users.unassigned')->with('users', User::all());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role_user  = Role::where('name', 'user')->first();

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $user = User::create([

            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt('password'),
        ]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user));
        $user->roles()->attach($role_user);

        return redirect()->route('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->back();
    }

    public function admin($id) {

        $user = User::find($id);

        $user->role_id = 1;

        $user->save();

        return redirect()->back();

    }


    public function not_admin($id) {

        $user = User::find($id);

        $user->role_id = 0;

        $user->save();

        return redirect()->back();

    }

    public function supervisor($id) {

        $user = User::find($id);

        $user->role_id = 2;

        $user->save();

        return redirect()->back();

    }

    public function agent($id) {

        $user = User::find($id);

        $user->role_id = 3;

        $user->save();

        return redirect()->back();

    }

    public function verify($id) {

        $user = User::find($id);

        $user->verified = 1;

        $user->save();

        return redirect()->back();

    }

}
