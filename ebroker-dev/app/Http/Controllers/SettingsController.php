<?php

namespace App\Http\Controllers;
use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }


    public function index() {
        return view('admin.settings.settings')->with('settings', Settings::first());
    }

    public function update()
    {

        $this->validate(request(), [
            'site_name' => 'required'

        ]);


        $settings = Settings::first();

        $settings->site_name = request()->site_name;

        $settings->save();

        return redirect()->back();
    }

}
