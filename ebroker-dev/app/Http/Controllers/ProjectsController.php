<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.projects.index')->with('projects', Project::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [

            'title' => 'required|max:255',
            'representation' => 'required',
            'state' => 'required'
        ]);


        $project = Project::create([
            'title' => $request->title,
            'representation' => $request->representation,
            'state' => $request->state,
            'slug' => str_slug($request->title),
            'user_id' => Auth::id()
        ]);


        return redirect()->route('projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        return view('admin.projects.edit')->with('project', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'representation' => 'required',
            'state' => 'required'
        ]);


        $project = Project::find($id);



        $project->title = $request->title;
        $project->representation = $request->representation;
        $project->state = $request->state;

        $project->save();


        return redirect()->route('projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project = Project::find($id);

        $project->delete();

        return redirect()->route('projects');

    }


    public function trashed()
    {
        $projects = Project::onlyTrashed()->get();


        return view('admin.projects.trashed')->with('projects', $projects);

    }


    public function kill($id)
    {
        $project = Project::withTrashed()->where('id', $id)->first();

        $project->forceDelete();

        return redirect()->route('trashed');

    }


    public function restore($id)
    {
        $post = Project::withTrashed()->where('id', $id)->first();

        $post->restore();

        return redirect()->route('projects');
    }
}
