<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return view('home')->with('user', $user);
    }

    public function admin()
    {
        return view('admin.dashboard')
            ->with('users', User::all())
            ->with('projects', Project::all());
    }

    public function supervisor()
    {
        return view('supervisor.dashboard')
            ->with('users', User::all())
            ->with('projects', Project::all());
    }
}
