<?php

namespace App\Http\Controllers;

use App\EstateLicense;
use App\User;
use App\Profile;
use App\Role;
use App\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProfilesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.profile')->with('user', Auth::user());
    }

    /* View user profile as Admin */
    public function view($id)
    {
        $user = User::find($id);
        $profile = Profile::where('user_id', '=', $id)->first();
        return view('admin.users.view')->with('profile', $profile)->with('user', $user);
    }

    public function edit_user_profile($id)
    {
        $user = User::find($id);
        $profile = Profile::where('user_id', '=', $id)->first();
        return view('admin.users.edit')->with('profile', $profile)->with('user', $user);
    }

    /* Editing user profile as Admin */
    public function update_user_profile(Request $request, $id)
    {

        $user = User::find($id);

        $profile = Profile::where('user_id', '=', $id)->first();


        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email'  => 'required|email',
            'address'  => 'required',
            'city'  => 'required',
            'state'  => 'required',
            'zip_code'  => 'required',
            'phone_number'  => 'required',
            'company_website'  => 'nullable'
        ]);


        // $user->email = $request->email;
        // $user->save();

        $profile->first_name = $request->first_name;
        $profile->last_name = $request->last_name;
        $profile->address = $request->address;
        $profile->city = $request->city;
        $profile->state = $request->state;
        $profile->zip_code = $request->zip_code;
        $profile->phone_number = $request->phone_number;
        $profile->company_website = $request->company_website;
        $profile->logo = $request->logo;

        $profile->save();


        if ($request->hasFile('logo'))
        {
            $logo = $request->logo;

            $logo_new_name = time() . $logo->getClientOriginalName();

            $logo->move('uploads/logos', $logo_new_name);

            $profile->logo = 'uploads/logos/' . $logo_new_name;

            $profile->save();
        }



        if($request->has('password'))
        {
            $user->password = bcrypt($request->password);

            $user->save();
        }


        return redirect()->back();
    }

    public function brokerLicense()
    {
        return view('agent.profile')->with('user', Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addBroker(Request $request)
    {
        $profile = Profile::create([
            'user_id' => Auth::id(),
            'license' => $request->license,
        ]);
        $userData = User::find($profile->user_id);

        //if the user supervisor
        if ($profile->license == 'license_supervisor') {
            

            
            /*
            $profile = Profile::findOrfail($profile)->update([
                'user_id' => Auth::id(),
                'license' => $request->license,
                'supervising_broker' => Auth::id(),
                'supervising_broker_name' => User::find($profile->name),
                'supervising_broker_email' => User::find($profile->email),

            ]);
            */
            
            return view('profile.contact-info')->with('profile', $profile)->with('profileId', $profile->id)->with('userData', $userData);
        }
        //if the user broker

        return view('profile.supervising-broker')->with('profile', $profile)->with('profileId', $profile->id)->with('userData', $userData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $user = Auth::user();

        $user->name = $request->name;

        $user->email = $request->email;

        $user->save();

        if ($request->filled('password')) {
            $user->password = bcrypt($request->password);

            $user->save();
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function fillprofile()
    {
        $user = User::all();

        return view('profile.home')->with('user', $user);
    }



/**
 * Start with multipages forms
 * Show the step 1 Form for creating a new product.
 *
 * @return \Illuminate\Http\Response
 */
    public function supervisor(Request $request)
    {
        // SLANJE EMAIL-A
        // Mail::to('potestati@gmail.com')->send(new EmailsController());
        
        //
        if($request->has('supervising_broker')){
       
            $this->validate($request, [

            'supervising_broker' => 'required|unique:profiles',
            ]);

        }else{
            $this->validate($request, [
            'supervising_broker_name' => 'required',
            'supervising_broker_email' => 'required|unique:profiles',
            ]);
        }

        $existingProfile = Profile::findOrfail($request->profile_id)->update([
            'supervising_broker' => $request->supervisor_account,
            'supervising_broker_name' => $request->supervisor_name,
            'supervising_broker_email' => $request->supervisor_email,

        ]);

        return view('profile.broker-setup')->with('profileId', $request->profile_id);
    }

    public function contactInfo(Request $request)
    {
        //profile_id je value pod atribut name u hidden field na prethodnom blade kod supervisor funkcije
        return view('profile.contact-info')->with('profileId', $request->profile_id);
    }

    public function licenseType(Request $request)
    {
        // return view('profile.test');
        
        
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip_code' => 'required',
            'phone_number' => 'required',
            'company_website' => 'required',
            'logo' => 'required',

        ]);
        //ovo je vezano za formu contact-info a posle submit se poziva blade license-type zato se tako zove f(x)
        $existingProfile = Profile::findOrFail($request->profile_id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip_code' => $request->zip_code,
            'phone_number' => $request->phone_number,
            'company_website' => $request->company_website,
            'logo' => $request->logo,
        ]);

        return view('profile.license-type')->with('profileId', $request->profile_id);
        // return view('profile.test');
    }

    public function termsServices(Request $request)
    {

        $state = $request->state;
        $license_company = $request->license_company;
        $license_number = $request->license_number;
        $ordinal_number = $request->ordinal_number;

        for ($i = 1; $i <= count($ordinal_number); $i++) {

            $estateLicense = new EstateLicense([
                'ordinal_number' => $ordinal_number[$i],
                'state' => $state[$i],
                'license_company' => $license_company[$i],
                'license_number' => $license_number[$i],
                'profile_id' => $request->profile_id,
            ]);
            $estateLicense->save();
        }

        // return view('profile.terms-services')->with('profileId', $request->profile_id);
        return view('profile.terms-services', compact('estateLicense'));

    }

    public function accountVerification(Request $request)
    {
        return view('agent.account-verification');
    }

}
