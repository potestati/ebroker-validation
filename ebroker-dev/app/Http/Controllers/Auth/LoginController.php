<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Role;
use App\User;
use App\Profile;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     *
     * @return mixed
     */
    protected function authenticated()
    {
        $profile = Profile::where('user_id', '=', Auth::id())->first();
        if(auth()->user()->hasRole('admin')) {
            return redirect()->route('dashboard');
        } elseif(auth()->user()->hasRole('supervisor')){
            return redirect()->route('supervisor.dashboard');
        } else {
            if (!auth()->user()->verified) {
                auth()->logout();
                return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
            }
            if(is_null($profile)) {
                return redirect()->route('fillprofile');
            } else {
                if($profile->terms_of_services !== 1) {
                    return redirect()->route('fillprofile'); // trebalo bi na step da ide gde je stao
                }
                
            }

            return redirect()->route('home');
        }
    }


}
