<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Settings;
use App\User;

Auth::routes();
Route::get('/', [
    'uses' => 'FrontEndController@index',
    'as' => 'home',
]);
Route::get('/about', function () {
    return view('about')->with('settings', Settings::first());
});
Route::get('/contact', function () {
    return view('contact')->with('settings', Settings::first());
});
/* started role login logic */
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/dashboard', 'HomeController@admin')->name('dashboard');

    Route::get('/users', [
        'uses' => 'UsersController@index',
        'as' => 'users',
    ]);

    Route::get('/unassigned', [
        'uses' => 'UsersController@unassigned',
        'as' => 'unassigned',
    ]);


    Route::any ( '/users/search', function () {
        $q = Input::get ( 'q' );
        $user = User::where ( 'name', 'LIKE', '%' . $q . '%' )->orWhere ( 'email', 'LIKE', '%' . $q . '%' )->get ();
        if (count ( $user ) > 0)
            return view ( 'admin.users.results' )->withDetails ( $user )->withQuery ( $q );
        else
            return view ( 'admin.users.results' )->withMessage ( 'No User found. Try to search again !' );
    } );


    Route::get('/user/delete/{id}', [
        'uses' => 'UsersController@destroy',
        'as' => 'user.delete',
    ]);

    Route::get('/user/create', [
        'uses' => 'UsersController@create',
        'as' => 'user.create',
    ]);

    Route::post('/user/store', [
        'uses' => 'UsersController@store',
        'as' => 'user.store',
    ]);

    Route::get('/user/admin/{id}', [
        'uses' => 'UsersController@admin',
        'as' => 'user.admin',
    ])->middleware('admin');

    Route::get('/user/not_admin/{id}', [
        'uses' => 'UsersController@not_admin',
        'as' => 'user.not_admin',
    ])->middleware('admin');

    Route::get('/user/verify/{id}', [
        'uses' => 'UsersController@verify',
        'as' => 'user.verify',
    ])->middleware('admin');

    Route::get('/settings', [
        'uses' => 'SettingsController@index',
        'as' => 'settings',
    ]);

    Route::post('/settings/update', [
        'uses' => 'SettingsController@update',
        'as' => 'settings.update',
    ]);
    // End of Admin group
    Route::get('/projects', [

        'uses' => 'ProjectsController@index',
        'as' => 'projects',

    ]);

    Route::get('/project/create', [

        'uses' => 'ProjectsController@create',
        'as' => 'project.create',

    ]);

    Route::post('/project/store', [

        'uses' => 'ProjectsController@store',
        'as' => 'project.store',

    ]);

    Route::get('/project/edit/{id}', [
        'uses' => 'ProjectsController@edit',
        'as' => 'project.edit',
    ]);

    Route::post('/project/update/{id}', [

        'uses' => 'ProjectsController@update',
        'as' => 'project.update',

    ]);

    Route::get('/project/delete/{id}', [
        'uses' => 'ProjectsController@destroy',
        'as' => 'project.delete',
    ]);

    Route::get('/trashed', [
        'uses' => 'ProjectsController@trashed',
        'as' => 'trashed',
    ]);

    Route::get('/project/kill/{id}', [
        'uses' => 'ProjectsController@kill',
        'as' => 'project.kill',
    ]);

    Route::get('/project/restore/{id}', [
        'uses' => 'ProjectsController@restore',
        'as' => 'project.restore',
    ]);

});

/* finished role login logic */
Route::get('/user/profile', [
    'uses' => 'ProfilesController@index',
    'as' => 'user.profile',
]);

Route::get('/user/profile/{id}', [
    'uses' => 'ProfilesController@view',
    'as' => 'user.profile.view'
])->middleware('admin');

Route::get('/user/profile/edit/{id}', [
    'uses' => 'ProfilesController@edit_user_profile',
    'as' => 'user.profile.edit_user_profile'
])->middleware('admin');

Route::post('/user/profile/updateprofile/{id}', [
    'uses' => 'ProfilesController@update_user_profile',
    'as' => 'user.profile.update_user_profile'
])->middleware('admin');


Route::post('/user/profile/update', [
    'uses' => 'ProfilesController@update',
    'as' => 'user.profile.update',
]);
//start multistep broker form
Route::get('/fillprofile', 'ProfilesController@fillprofile')->name('fillprofile');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/home/broker', 'ProfilesController@brokerLicense')->name('brokerLicense');
//create licence broker form
Route::post('/home/broker', 'ProfilesController@addBroker')->name('addBroker');
//after store licence broker form page
Route::post('/home/broker/supervisor', 'ProfilesController@supervisor')->name('supervisor');//ova ruta salje email
//edit licence broker form
Route::get('/home/broker/contact-info', 'ProfilesController@contactInfo')->name('contactInfo');
//dropdown entering info about broker license , type of broker license
Route::post('/home/broker/license-type', 'ProfilesController@licenseType')->name('licenseType');
Route::post('/home/broker/terms-services', 'ProfilesController@termsServices')->name('termsServices');
Route::get('/home/broker/account-verification', 'ProfilesController@accountVerification')->name('verification');

// login role for supervisor
Route::group(['prefix' => 'supervisor', 'middleware' => ['auth', 'supervisor']], function () {

    Route::get('/dashboard', 'HomeController@supervisor')->name('supervisor.dashboard');

    Route::get('/team', [
        'uses' => 'TeamController@index',
        'as' => 'team',
    ]);

    Route::get('/team/view/{id}', [
        'uses' => 'TeamController@view',
        'as' => 'supervisor.team.view'
    ]);

});

//start multistep supervosor form
// Route::get('/home/broker',
