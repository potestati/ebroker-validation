@extends('layouts.supervisor')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">My Team Dashboard     <a href="#" class="float-right btn btn-sm btn-primary">Invite Agent</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Date registered</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member->name }}</td>
                                    <td>{{ $member->email }}</td>
                                    <td>{{ $member->roles()->first()->name }}</td>
                                    <td>{{ $member->created_at }}</td>
                                    <td>
                                      @if($member->verified)
                                                Verified
                                            @else
                                                Not Verified
                                      @endif
                                    </td>
                                    <td>
                                            <a href="{{ route('supervisor.team.view', ['id' => $member->id])  }}" class="btn btn-sm btn-info">View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </tbody>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
