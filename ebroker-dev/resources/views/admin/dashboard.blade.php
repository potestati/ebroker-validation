@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Admin Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <p>You are logged in!</p>
                            <p>There are {{ $users->count() }} users registered and {{ $projects->count() }} projects created.</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
