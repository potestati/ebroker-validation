@extends('layouts.admin')



@section('content')

    @if(count($errors) > 0)

        <ul class="list-group">

            @foreach($errors->all() as $error)

                <li class="list-group-item text-danger">

                    {{$error}}

                </li>
            @endforeach

        </ul>

    @endif

    <div class="card">

        <div class="card-header">

            Edit your profile

        </div>
        <div class="card-body">
            <form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">

                {{csrf_field()}}

                <div class="form-group">

                    <label for="name">Username</label>

                    <input type="text" class="form-control" value="{{ $user->name }}" name="name">

                </div>


                <div class="form-group">

                    <label for="email">Email</label>

                    <input type="email" class="form-control" value="{{ $user->email }}"  name="email">

                </div>


                <div class="form-group">

                    <label for="password">New Password</label>

                    <input type="password" class="form-control" name="password">

                </div>




                <div class="form-group">

                    <div class="text-center">

                        <button class="btn btn-success" type="submit">Update Profile</button>
                    </div>
                </div>

            </form>
        </div>
    </div>





@stop
