@extends('layouts.admin') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Users Dashboard <a href="{{ route('users') }}" class="float-right btn btn-sm btn-primary">Back to Users</a>
                </div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Verified</th>
                                <th>Email</th>
                                <th>View Profile</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            @if(is_null($user->profile()->first()->supervising_broker) and !$user->hasRole('supervisor') and !$user->hasRole('admin') )
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>
                                   {{ $user->roles()->first()->name }}
                                </td>
                                <td>
                                    @if($user->verified) Verified @else
                                    <a href="{{ route('user.verify', ['id' => $user->id])  }}" class="btn btn-sm btn-success">Verify</a>                                    @endif
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ route('user.profile.view', ['id' => $user->id])  }}" class="btn btn-sm btn-info">View</a>
                                </td>
                                <td>
                                    <a href="{{ route('user.delete', ['id' => $user->id])  }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>






                    </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection