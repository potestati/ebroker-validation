@extends('layouts.admin') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                
                <div class="card-header">Users Dashboard 
                    <a href="{{ route('user.create') }}" class="float-right btn btn-sm btn-primary">Add User</a>
                    <a href="{{ route('unassigned') }}" class="float-right btn btn-sm mr-1 btn-warning">Unassigned users</a>
                </div>
                <div class="col-md-12 mt-3">
                <form action="/admin/users/search" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="input-group">
                            <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="q" placeholder="Search users" aria-label="Search users" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                      <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                                    </div>
                                  </div>
                        </span>
                    </div>
                </form>
            </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Team</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Verified</th>
                                <th>Email</th>
                                <th>View Profile</th>
                                <th>Delete</th>
                            </tr>
                        </thead>


                        @foreach($users as $agent) 
                        @if($agent->hasRole('supervisor'))
                        <tbody>
                               
                            <tr class="clickable" data-toggle="collapse" data-target="#group-of-rows-{{ $loop->iteration }}" aria-expanded="false" aria-controls="group-of-rows-{{ $loop->iteration }}">
                                <td><i class="fa fa-plus" aria-hidden="true"></i></td>
                                <td>{{ $agent->name }}</td>
                                <td>{{ $agent->roles()->first()->name }}</td>
                                <td>
                                    @if($agent->verified) Verified @else
                                    <a href="{{ route('user.verify', ['id' => $agent->id])  }}" class="btn btn-sm btn-success">Verify</a>                                    @endif
                                </td>
                                <td>{{ $agent->email }}</td>
                                <td>
                                    <a href="{{ route('user.profile.view', ['id' => $agent->id])  }}" class="btn btn-sm btn-info">View</a>
                                </td>
                                <td>
                                    <a href="{{ route('user.delete', ['id' => $agent->id])  }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="group-of-rows-{{ $loop->iteration }}" class="collapse">

                            @foreach($users as $user) 
                            @if($user->profile()->first()->supervising_broker == $agent->id)
                            <tr>
                                <td></td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->roles()->first()->name }}</td>
                                <td>
                                    @if($user->verified) Verified @else
                                    <a href="{{ route('user.verify', ['id' => $user->id])  }}" class="btn btn-sm btn-success">Verify</a>  @endif
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ route('user.profile.view', ['id' => $user->id])  }}" class="btn btn-sm btn-info">View</a>
                                </td>
                                <td>
                                    <a href="{{ route('user.delete', ['id' => $user->id])  }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>


                        @endif
                        @endforeach

                    </table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection