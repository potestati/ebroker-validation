@extends('layouts.admin') 
@section('content') @if(count($errors) > 0)

<ul class="list-group">

    @foreach($errors->all() as $error)

    <li class="list-group-item text-danger">

        {{$error}}

    </li>
    @endforeach

</ul>

@endif

<div class="card">

    <div class="card-header">

        Edit Users Profile

    </div>
    <div class="card-body">
        <form action="{{ route('user.profile.update_user_profile', ['id' => $user->id]) }}" method="post" enctype="multipart/form-data">

            {{csrf_field()}}

            <div class="form-group">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" value="{{ $profile->first_name }}" name="first_name">
            </div>

            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" value="{{ $profile->last_name }}" name="last_name">
            </div>

            <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" value="{{ $user->email }}" name="email">
            </div>

            <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" value="{{ $profile->address }}" name="address">
            </div>


            <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" class="form-control" value="{{ $profile->city }}" name="city">
            </div>


            
            <div class="form-group">
                    <label for="state">State</label>
                    <input type="text" class="form-control" value="{{ $profile->state }}" name="state">
            </div>


            
            <div class="form-group">
                    <label for="zip_code">Zip</label>
                    <input type="text" class="form-control" value="{{ $profile->zip_code }}" name="zip_code">
            </div>

            
            <div class="form-group">
                    <label for="phone_number">Phone</label>
                    <input type="text" class="form-control" value="{{ $profile->phone_number }}" name="phone_number">
            </div>


            @if($user->hasRole('supervisor'))
            <div class="form-group">
                    <label for="company_website">Company website</label>
                    <input type="text" class="form-control" value="{{ $profile->company_website }}" name="company_website">
            </div>

            @if($profile->logo)
            <p>Current logo</p>
            <img src="{{ asset($profile->logo) }}" width="100px" height="auto" alt="Current logo">
            @endif
            <div class="form-group">
                    <label for="logo">Replace logo</label>
                    <input type="file" class="form-control" name="logo">
            </div>
            @endif

            
            <div class="form-group">
                    <label for="password">Change password</label>
                    <input type="password" class="form-control" name="password">
            </div>

            <div class="form-group">

                <div class="text-center">

                    <button class="btn btn-success" type="submit">Update users profile</button>
                </div>
            </div>

        </form>
    </div>
</div>






@stop