@extends('layouts.admin') 
@section('content')
@if(isset($details))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Users Dashboard <a href="{{ route('users') }}" class="float-right btn btn-sm btn-primary">Back to Users</a>
                </div>
                <div class="card-body">
                   <div class="col-md-12">
                        <p>The Search results for your query <b> {{ $query }} </b> are :</p>  
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Verified</th>
                                <th>Email</th>
                                <th>View Profile</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($details as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>
                                   {{ $user->roles()->first()->name }}
                                </td>
                                <td>
                                    @if($user->verified) Verified @else
                                    <a href="{{ route('user.verify', ['id' => $user->id])  }}" class="btn btn-sm btn-success">Verify</a>                                    @endif
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ route('user.profile.view', ['id' => $user->id])  }}" class="btn btn-sm btn-info">View</a>
                                </td>
                                <td>
                                    <a href="{{ route('user.delete', ['id' => $user->id])  }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(isset($message))
<div class="card">
        <div class="card-header">Users Dashboard <a href="{{ route('users') }}" class="float-right btn btn-sm btn-primary">Back to Users</a>
        </div>
        <div class="card-body">
                <p>{{ $message }}</p>
                <div class="col-md-12 mt-3">
                        <form action="/admin/users/search" method="POST" role="search">
                            {{ csrf_field() }}
                            <div class="input-group">
                                    <div class="input-group mb-3">
                                            <input type="text" class="form-control" name="q" placeholder="Search users" aria-label="Search users" aria-describedby="button-addon2">
                                            <div class="input-group-append">
                                              <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                                            </div>
                                          </div>
                                </span>
                            </div>
                        </form>
                    </div>
            </div>          

@endif
@endsection