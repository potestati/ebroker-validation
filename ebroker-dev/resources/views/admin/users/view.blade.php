@extends('layouts.admin') 
@section('content') @if(count($errors) > 0)

<ul class="list-group">

    @foreach($errors->all() as $error)

    <li class="list-group-item text-danger">

        {{$error}}

    </li>
    @endforeach

</ul>

@endif

<div class="card">
    <div class="card-header">

        View user profile
        <a href="{{ route('user.profile.edit_user_profile', ['id' => $user->id])  }}" class="float-right btn btn-sm btn-primary">Edit User Profile</a>
        <a href="{{ route('users') }}" class="float-right btn btn-sm btn-primary mr-1">Back to Users</a>
    </div>
    <div class="card-body">
            <div class="row">
            <div class="col-md-6">
                    <div class="profile-head">
                                <h5>
                                        {{ $profile->first_name }} {{ $profile->last_name }}
                                </h5>
                                <h6>
                                        {{ $user->email }}
                                </h6>
                                <h6>
                                   Role:  {{ $user->roles()->first()->name }}
                                </h6>                             
                    </div>
                </div>
            </div>


                <div class="row">
                            <div class="col-12">
                                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">User Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#connectedServices" role="tab" aria-controls="connectedServices" aria-selected="false">Projects working on</a>
                                    </li>
                                </ul>
                                <div class="tab-content ml-1" id="myTabContent">
                                    <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
               
                                        <div class="row border-bottom mb-3">
                                            <div class="col-4">
                                                <label style="font-weight:bold;">Registered</label>
                                            </div>
                                            <div class="col-8">
                                                    {{ $user->created_at->toDayDateTimeString() }}
                                            </div>
                                        </div>

                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Verification status</label>
                                            </div>
                                            <div class="col-8">
                                                    @if($user->verified) Verified @else
                                                    <a href="{{ route('user.verify', ['id' => $agent->id])  }}" class="btn btn-sm btn-success">Verify</a>
                                                    @endif
                                            </div>
                                        </div>
 
                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Address</label>
                                            </div>
                                            <div class="col-8">
                                                  {{ $profile->address}}
                                            </div>
                                        </div>
                                        
                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">City</label>
                                            </div>
                                            <div class="col-8">
                                                  {{ $profile->city}}
                                            </div>
                                        </div>

                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">State</label>
                                            </div>
                                            <div class="col-8">
                                                  {{ $profile->state}}
                                            </div>
                                        </div>


                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Zip</label>
                                            </div>
                                            <div class="col-8">
                                                  {{ $profile->zip_code}}
                                            </div>
                                        </div>

                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Phone</label>
                                            </div>
                                            <div class="col-8">
                                                  {{ $profile->phone_number}}
                                            </div>
                                        </div>


                                        @if($user->hasRole('supervisor'))
                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Company website</label>
                                            </div>
                                            <div class="col-8">
                                                 <a href="{{ $profile->company_website}}">{{ $profile->company_website}}</a>
                                            </div>
                                        </div>

                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Company logo</label>
                                            </div>
                                            <div class="col-8">
                                                TO-DO
                                            </div>
                                        </div>
                                        @endif


                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Terms of Service</label>
                                            </div>
                                            <div class="col-8">
                                                           @if($profile->terms_of_services)
                                                                Signed
                                                           @else
                                                                Not signed
                                                           @endif
                                            </div>
                                        </div>


                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Profile created</label>
                                            </div>
                                            <div class="col-8">
                                                    {{ $profile->created_at->toDayDateTimeString() }}

                                            </div>
                                        </div>

                                        <div class="row border-bottom mb-3">
                                                <div class="col-4">
                                                <label style="font-weight:bold;">Profile updated</label>
                                            </div>
                                            <div class="col-8">
                                                    {{ $profile->updated_at->toDayDateTimeString('Y-m-d H') }}

                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="connectedServices" role="tabpanel" aria-labelledby="ConnectedServices-tab">
                                        TO-DO
                                    </div>
                                </div>
                            </div>
                        </div>

    </div>
</div>






@stop