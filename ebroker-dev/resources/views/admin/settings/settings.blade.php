@extends('layouts.admin')

@section('content')

    @if(count($errors) > 0)

        <ul class="list-group">

            @foreach($errors->all() as $error)

                <li class="list-group-item text-danger">

                    {{$error}}

                </li>
            @endforeach

        </ul>

    @endif

    <div class="card">

        <div class="card-header">

            Settings

        </div>
        <div class="card-body">
            <form action="{{ route('settings.update') }}" method="post">

                {{csrf_field()}}

                <div class="form-group">

                    <label for="site_name">Site name</label>

                    <input type="text" class="form-control" value="{{ $settings->site_name }}" name="site_name">

                </div>

                <div class="form-group">

                    <div class="text-center">

                        <button class="btn btn-success" type="submit">Update site settings</button>
                    </div>
                </div>

            </form>
        </div>
    </div>





@stop
