@extends('layouts.admin')

@section('content')

    <div class="card">
        <div class="card">
            <div class="card-header">Projects     <a href="{{ route('project.create') }}" class="float-right btn btn-sm btn-primary">Add Project</a>
            </div>
        <div class="card-body">
            <table class="table table-hover">

                <thead>

                <th>
                    Name
                </th>
                <th>
                    Status
                </th>
                <th>
                    Edit
                </th>
                <th>
                    Delete
                </th>
                </thead>

                <tbody>

                @foreach($projects as $project)

                    <tr>

                        <td>
                            {{ $project->title }}
                        </td>
                        <td>
                            @if($project->status = 'active')
                                <span class="badge badge-success">Active</span>
                            @elseif($project->status = 'queued')
                                <span class="badge badge-warning">Queued</span>
                            @else
                                <span class="badge badge-primary">Completed</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('project.edit', ['id' => $project->id])  }}" class="btn btn-sm btn-primary">Edit</a>
                        </td>
                        <td>
                            <a href="{{ route('project.delete', ['id' => $project->id])  }}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>
            @stop
        </div>
    </div>
