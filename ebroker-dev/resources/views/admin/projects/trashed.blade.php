@extends('layouts.admin')

@section('content')

    <div class="card">
        <div class="card">
            <div class="card-header">Projects
            </div>
            <div class="card-body">
                <table class="table table-hover">

                    <thead>

                    <th>
                        Name
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                        Delete
                    </th>
                    </thead>

                    <tbody>

                    @foreach($projects as $project)

                        <tr>

                            <td>
                                {{ $project->title }}
                            </td>
                            <td>
                                @if($project->status = 'active')
                                    <span class="badge badge-success">Active</span>
                                @elseif($project->status = 'queued')
                                    <span class="badge badge-warning">Queued</span>
                                @else
                                    <span class="badge badge-primary">Completed</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('project.restore', ['id' => $project->id])  }}" class="btn btn-sm btn-primary">Restore</a>
                            </td>
                            <td>
                                <a href="{{ route('project.kill', ['id' => $project->id])  }}" class="btn btn-sm btn-danger">Delete</a>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
                @stop
            </div>
        </div>
