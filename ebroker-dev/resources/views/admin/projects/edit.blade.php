@extends('layouts.admin')

@section('content')

@if(count($errors) > 0)

<ul class="list-group">

    @foreach($errors->all() as $error)

    <li class="list-group-item text-danger">

        {{$error}}

    </li>
    @endforeach

</ul>

@endif

<div class="card">

    <div class="card-header">

        Edit project

    </div>
    <div class="card-body">
        <form action="{{ route('project.update',  ['id' => $project->id ]) }}" method="post">

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Title</label>

                <input type="text" class="form-control" name="title" value="{{ $project->title }}">

            </div>



            <div class="form-group">
                <label for="state">State</label>
                {{

                echo Form::select('state',array(
                'AL'=>'Alabama',
                'AK'=>'Alaska',
                'AZ'=>'Arizona',
                'AR'=>'Arkansas',
                'CA'=>'California',
                'CO'=>'Colorado',
                'CT'=>'Connecticut',
                'DE'=>'Delaware',
                'DC'=>'District of Columbia',
                'FL'=>'Florida',
                'GA'=>'Georgia',
                'HI'=>'Hawaii',
                'ID'=>'Idaho',
                'IL'=>'Illinois',
                'IN'=>'Indiana',
                'IA'=>'Iowa',
                'KS'=>'Kansas',
                'KY'=>'Kentucky',
                'LA'=>'Louisiana',
                'ME'=>'Maine',
                'MD'=>'Maryland',
                'MA'=>'Massachusetts',
                'MI'=>'Michigan',
                'MN'=>'Minnesota',
                'MS'=>'Mississippi',
                'MO'=>'Missouri',
                'MT'=>'Montana',
                'NE'=>'Nebraska',
                'NV'=>'Nevada',
                'NH'=>'New Hampshire',
                'NJ'=>'New Jersey',
                'NM'=>'New Mexico',
                'NY'=>'New York',
                'NC'=>'North Carolina',
                'ND'=>'North Dakota',
                'OH'=>'Ohio',
                'OK'=>'Oklahoma',
                'OR'=>'Oregon',
                'PA'=>'Pennsylvania',
                'RI'=>'Rhode Island',
                'SC'=>'South Carolina',
                'SD'=>'South Dakota',
                'TN'=>'Tennessee',
                'TX'=>'Texas',
                'UT'=>'Utah',
                'VT'=>'Vermont',
                'VA'=>'Virginia',
                'WA'=>'Washington',
                'WV'=>'West Virginia',
                'WI'=>'Wisconsin',
                'WY'=>'Wyoming',
                ), $project->state, [
                'class' => 'form-control',
                'placeholder' => 'Select State'
                ]);
                }}
            </div>


            <div class="form-group">

                <label for="representation">Representation</label>

                <select name="representation" id="representation" class="form-control">
                    <option value="buyer" @if($project->representation == 'buyer')
                        selected="selected"
                        @endif
                        >Buyer</option>
                    <option value="seller" @if($project->representation == 'seller')
                        selected="selected"
                        @endif>Seller</option>
                </select>

            </div>


            <div class="form-group">


                <div class="text-center">

                    <button class="btn btn-success" type="submit">Update Project</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>
</div>


@stop
