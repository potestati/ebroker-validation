@include('includes/header')


<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">About</h1>
    <p class="lead">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It's built with default Bootstrap components and utilities with little customization.</p>
</div>

<div class="container">

        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis ligula libero. Nunc semper congue molestie. Praesent congue mollis est vel vulputate. Suspendisse quis porta diam. Sed sit amet felis urna. Vivamus porta tincidunt lacus non aliquet. Curabitur venenatis viverra orci id fringilla. Donec ipsum purus, venenatis ut magna non, ultricies bibendum lacus. Proin ipsum felis, placerat et magna at, consequat porta nulla. Cras hendrerit dictum aliquam. Donec non urna vel nisi mollis posuere. Maecenas tempor molestie felis sodales feugiat. Mauris id neque eu ante varius pharetra. Morbi posuere faucibus magna.
        </p>
        <p>
            Sed semper vestibulum commodo. Sed laoreet, nibh eu sagittis sollicitudin, neque massa consectetur justo, eu laoreet felis metus placerat sapien. Quisque sit amet felis a metus ultrices fringilla in sit amet nisi. Nulla eleifend, odio eu laoreet consectetur, ex nulla tristique massa, et suscipit nunc sapien a elit. Sed ac laoreet tellus. Fusce tempor finibus gravida. Nunc ac ante eget mauris luctus sodales. Vivamus aliquet auctor leo sit amet pharetra. Etiam sed sapien diam. Curabitur nulla dolor, pretium sit amet posuere ut, pharetra vitae dolor. Quisque porta velit augue, a efficitur odio gravida nec. Suspendisse dapibus augue velit, sit amet commodo metus dapibus et. Suspendisse mattis risus et ante maximus, non imperdiet est vehicula.
        </p>
        <p>
            Mauris faucibus mauris at dui viverra convallis. Cras in ex vehicula, porttitor mauris in, porttitor velit. Etiam accumsan molestie nisl sit amet faucibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce vel lectus sit amet ipsum rhoncus varius id et eros. Nullam at dolor accumsan, molestie sapien ut, viverra lectus. Nulla facilisi. Praesent gravida nisl a turpis pellentesque elementum. Nunc eget mi urna. Nulla facilisi. Cras vitae libero feugiat, condimentum risus id, convallis est. Quisque ut tellus vitae ex maximus accumsan sit amet ut neque. Phasellus scelerisque iaculis sapien, vitae pretium enim commodo sit amet. Vivamus iaculis turpis vitae turpis lacinia, ac viverra erat porttitor.
        </p>
        <p>
            Sed ornare rhoncus justo, id placerat dolor auctor at. Integer ipsum enim, dapibus sed convallis vitae, lacinia sit amet ante. Suspendisse diam tellus, commodo id mattis aliquam, varius ac odio. Nullam mollis, eros eu rhoncus eleifend, nunc diam porta ante, in molestie lorem tortor at risus. Etiam tristique turpis ut felis tristique, at tempor lacus ornare. Nulla molestie nec metus a finibus. Sed blandit mi eu tortor suscipit, ac cursus lacus aliquam. Nulla arcu lectus, pulvinar at augue et, mollis gravida quam. Donec mollis eros pretium nunc fermentum vehicula. Cras consequat arcu risus, eu porttitor lorem congue id. Aliquam efficitur eros sit amet nisl laoreet blandit.
        </p>
        <p>
            Mauris aliquam augue id urna commodo, eget gravida elit sollicitudin. Etiam luctus molestie tellus, vel aliquet nunc vestibulum sit amet. Morbi et sollicitudin nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus efficitur maximus quam id iaculis. Cras non leo vitae nunc fermentum vestibulum. Sed rhoncus purus a tellus elementum, in volutpat mi venenatis. Fusce libero libero, pellentesque id vehicula vel, tempor nec enim. Cras gravida dapibus consectetur.
        </p>
 </div>

@include('includes.footer')