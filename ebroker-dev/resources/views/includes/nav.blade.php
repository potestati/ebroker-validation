<nav class="navbar navbar-expand-sm navbar-light bg-faded">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

      <!-- Brand -->
      <a class="navbar-brand" href="#">Company Logo</a>

      <!-- Links -->
        <div class="collapse navbar-collapse" id="nav-content">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/">HOME</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/about">ABOUT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact">CONTACT US</a>
            </li>
            @if(!Auth::check())
            <li class="nav-item">
              <a class="nav-link" href="/login">LOGIN</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/register">SING UP</a>
            </li>
           <!-- admin -->
              @elseif(Auth::user()->role_id == 1)
              <li class="panel-info"><a class="btn btn-light" href="{{ route('dashboard') }}">Admin Panel</a></li>
          <!-- supervisor -->
              @elseif(Auth::user()->role_id == 2)
              <li class="panel-info"><a class="btn btn-light" href="{{ route('dashboard') }}">Supervisor Panel</a></li>
          <!-- agent -->
          @elseif(Auth::user()->role_id == 3)
              <!-- @else -->
              <li class="panel-info"><a class="btn btn-light" href="{{ route('dashboard') }}">Agent Panel</a></li>
              @endif

            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              {{ __('LOGOUT') }}
            </a>
            </li>

          </ul>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </div>
      </nav>
