@extends('layouts.profile')

@section('content')

<div class="card">
    <div class="card-body">
        You’re all finished. We are currently verifying
        your account information. You will receive an
        email confirmation as soon as we have
        completed our review.
    </div>
</div>


@stop
