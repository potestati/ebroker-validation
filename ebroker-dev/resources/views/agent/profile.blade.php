@extends('layouts.profile')

@section('content')
<div class="card">
    <div class="card-header">
        <h1>Hello {{ $user->name }}</h1>
        <h4>you are loged in with role {{ $user->role_id }} and your user id is {{ $user->id }}</h4>
    </div>
    <div class="card-body">
        <form action="{{ route('addBroker') }}" method="POST">
            <!-- <input name="prosledjenUserId" id="prosledjenUserId" type="hidden" value="  $user->id }}"> -->
            {{ csrf_field() }}
            <input type="radio" name="license" value="license_supervisor">
            <p>
                I am a licensed real estate broker. I am not required to hang
                my license with a supervising broker and I am not currently
                hanging my license with a supervising broker.
            </p>
            <br>
            <input type="radio" name="license" value="license_broker">
            <p>
                I am a licensed real estate agent and I am required to hang
                my license with a supervising broker
                <br>
                or
                <br>
                I am a licensed real estate broker and I am currently hanging
                my license with a supervising broker
            </p>
            <button type="submit" class="btn btn-primary">Continue</button>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </form>
    </div>
    <div class="card-footer">Example Footer Information</div>
</div>
@stop
