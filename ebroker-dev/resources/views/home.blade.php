@extends('layouts.profile')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card bg-info text-white">
                <div class="card-body">
                    <p>Thank you. Your email has been verified.</p>
                    <p>Please select continue to finish creating your new account</p>
                    <a href="{{ route('brokerLicense') }}">Continue</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
