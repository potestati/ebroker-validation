@extends('layouts.profile')

@section('content')
@php (
$states=[
'Alabama',
'Alaska',
'Arizona',
'Arkansas',
'California',
'Colorado',
'Connecticut',
'Delaware',
'Florida',
'Georgia',
'Hawaii',
'Idaho',
'Illinois',
'Indiana',
'Iowa',
'Kansas',
'Kentucky',
'Louisiana',
'Maine',
'Maryland',
'Massachusetts',
'Michigan',
'Minnesota',
'Mississippi',
'Missouri',
'Montana',
'Nebraska',
'Nevada',
'New Hampshire',
'New Jersey',
'New Mexico',
'New York',
'North Carolina',
'North Dakota',
'Ohio',
'Oklahoma',
'Oregon',
'Pennsylvania',
'Rhode Island',
'South Carolina',
'South Dakota',
'Tennessee',
'Texas',
'Utah',
'Vermont',
'Virginia',
'Washington',
'West Virginia',
'Wisconsin',
'Wyoming',
] )
profile id : 
{{$profileId}}<br>
profile : //$profile}}<br>
userData : //$userData}}<br>
<div class="card bg-success text-white">
    <div class="card-body">
        <h1>
            Please enter your contact information below
        </h1>
    </div>
</div>
<div class="card bg-light text-dark">
    <div class="card-body">
        <form method="POST" action="{{ route('licenseType') }}">
            <!-- ovde ga pokupi iz Profile kontrolera funkcija contactInfo i bice dalje pokupljeno sledecim metodom licenseType iz Provile kontrolera-->
            <input type="hidden" name="profile_id" value="{{ $profileId }}">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="John">
                </div>
                <div class="form-group col-md-6">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Smith">
                </div>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <input type="text" name="address" class="form-control" id="address" placeholder="1234 Main St">
            </div>
            <div class="form-group">
                <label for="address2">Address 2</label>
                <input type="text" name="address2" class="form-control" id="address2" placeholder="Apartment, studio, or floor">
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="city">City</label>
                    <input type="text" name="city" class="form-control" id="city">
                </div>
                <div class="form-group col-md-4">
                    <label for="state">State</label>
                    <select id="state" name="state" class="form-control">
                        <option selected>Choose...</option>
                        @foreach($states as $key=>$state)
                        <option value="{{ $state }}">{{ $state }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="zip_code">Zip</label>
                    <input type="text" name="zip_code" class="form-control" id="zip_code">
                </div>
            </div>
            <div class="form-group">
                <label for="phone_number">Phone Number</label>
                <input type="tel" name="phone_number" class="form-control" id="phone_number" placeholder="123-456-7890"
                    required>
            </div>
            <div class="form-group row">
                <label for="example-url-input" class="col-12 col-form-label">Please enter the URL Address of the
                    Company
                    Website</label>
                <div class="col-12">
                    <input class="form-control" name="company_website" type="url" value="https://www.apple.com" id="example-url-input">
                </div>
            </div>
            <div class="form-group">
                <label for="logo">Select File to Upload Logo</label>
                <input type="file" name="logo" class="form-control-file" id="logo">
            </div>
            <button type="submit" class="btn btn-primary">Continue</button>
        </form>
    </div>
</div>


@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


@stop
