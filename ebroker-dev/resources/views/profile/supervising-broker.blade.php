@extends('layouts.profile')

@section('content')

{{$profile}}
{{$profileId}}

Here is the whole profile license data {{ $profile->license }}

<br>
Here is the profile {{ $profile }}
<div class="card bg-success text-white">
    <div class="card-body">
        <h1>Hello you are Agent/Broker and your ID no. is {{ $userId = $profile->user_id }}</h1>
    </div>
</div>
<div class="card bg-light text-dark">
    <div class="card-body">
        <h3>
            Please make a selection below about your Supervising Broker,
            <br>
            fill in the appropriate
            information, and select continue:
        </h3>
    </div>
</div>

<form action="{{ route('supervisor') }}" method="POST">
    <input type="hidden" name="profile_id" value="{{ $profileId }}">
    {{ csrf_field() }}
    <fieldset class="form-group">
        <label for="supervisor_account">
            My Supervising Broker has an account.
        </label>
        <input type="text" class="form-control" id="supervisor_account" name="supervising_broker" placeholder="Enter Account Number">
    </fieldset>
    <fieldset class="form-group">
        <label for="supervisor_name">
            Invite my Supervising Broker to create an account so I may use
            your services.
        </label>
        <input type="text" class="form-control" id="supervisor_name" name="supervising_broker_name" placeholder="Supervising Broker Name">
    </fieldset>
    <fieldset class="form-group">
        <input type="email" class="form-control" id="supervisor_email" name="supervising_broker_email" placeholder="Supervising Broker Email">
    </fieldset>
    <button type="submit" class="btn btn-primary">Continue</button>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</form>

<script>
jQuery( document ).ready(function() {
jQuery('#supervisor_account').keyup(function(){
if(jQuery(this).val()) {
jQuery('#supervisor_name').prop('disabled',true);
jQuery('#supervisor_email').prop('disabled',true);
} else {
jQuery('#supervisor_name').prop('disabled',false);
jQuery('#supervisor_email').prop('disabled',false);
}
});
jQuery('#supervisor_name').keyup(function(){
if(jQuery(this).val()) {
jQuery('#supervisor_account').prop('disabled',true);
} else {
jQuery('#supervisor_account').prop('disabled',false);
}
});
jQuery('#supervisor_email').keyup(function(){
if(jQuery(this).val()) {
jQuery('#supervisor_account').prop('disabled',true);
} else {
jQuery('#supervisor_account').prop('disabled',false);
}
});
});
</script>

@stop
