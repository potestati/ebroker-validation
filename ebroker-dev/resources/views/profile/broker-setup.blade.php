@extends('layouts.profile')

@section('content')
<div class="card bg-info text-white">
    <div class="card-body">
        <p>
            An invitation email has been sent to your Supervising Broker. You may proceed forward with your account
            setup,
            however you will not be able to begin using our services until your Supervising Broker has successfully
            setup an
            account and associated it with your account.
        </p>
        <form action="{{ route('contactInfo') }}" method="GET">
            {{ csrf_field() }}
            <input type="hidden" name="profile_id" value="{{ $profileId }}">

            <button type="submit" class="btn btn-primary">Continue</button>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </form>
    </div>
</div>


@stop
