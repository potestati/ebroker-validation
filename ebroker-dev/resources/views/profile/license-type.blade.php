@extends('layouts.profile')

@section('content')
@php (
$states=[
'Alabama',
'Alaska',
'Arizona',
'Arkansas',
'California',
'Colorado',
'Connecticut',
'Delaware',
'Florida',
'Georgia',
'Hawaii',
'Idaho',
'Illinois',
'Indiana',
'Iowa',
'Kansas',
'Kentucky',
'Louisiana',
'Maine',
'Maryland',
'Massachusetts',
'Michigan',
'Minnesota',
'Mississippi',
'Missouri',
'Montana',
'Nebraska',
'Nevada',
'New Hampshire',
'New Jersey',
'New Mexico',
'New York',
'North Carolina',
'North Dakota',
'Ohio',
'Oklahoma',
'Oregon',
'Pennsylvania',
'Rhode Island',
'South Carolina',
'South Dakota',
'Tennessee',
'Texas',
'Utah',
'Vermont',
'Virginia',
'Washington',
'West Virginia',
'Wisconsin',
'Wyoming',
] )
<div class="card bg-primary text-white">
    <div class="card-body">
        <h1>Please enter the information below for your
            real estate license(s)</h1>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form action="{{ route('termsServices') }}" method="POST" class="license-type-form">
            {{ csrf_field() }}
            <!-- polje profile id ce se preneti dalje kako bi se po njemu u novoj tabeli estate_license po profile_id znalo koji profil ima te licence-->
            <input type="hidden" name="profile_id" value="{{ $profileId }}">
            <input type="hidden" name="ordinal_number" value="{{ $ordinalNumber = 1}}">
            <h4>Primary License:</h4>

            <div style="float: left; width: 100%;">
                <input type="hidden" name="ordinal_number[1]" value="1">
                <div class="form-group col-sm-3" style="float: left;">
                    <!-- State input-->
                    <label class="control-label " for="state[1]">State</label>
                    <select class="form-control" id="state[1]" name="state[1]">
                        <option selected>State Dropdown</option>
                        @foreach($states as $key=>$state)
                        <option value="{{ $state }}">{{ $state }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-4" style="float: left;">
                    <!-- Company License-->
                    <label for="license_company[1]" class="control-label"> Name on License</label>
                    <input type="text" class="form-control" id="license_company[1]" name="license_company[1]"
                        placeholder="Use Company Name if Applicable">
                </div>
                <div class="form-group col-sm-4" style="float: left;">
                    <!-- Licesne Number-->
                    <label class="control-label " for="license_number[1]">License Number</label>
                    <textarea class="form-control" id="license_number[1]" name="license_number[1]" rows="1"></textarea>
                </div>
            </div>

            <h4>Additional License:</h4>
            <div id="additional-license">
                <div style="float: left; width: 100%;">
                    <input type="hidden" name="ordinal_number[2]" value="2">
                    <div class="form-group col-sm-3" style="float: left;">
                        <!-- State input-->
                        <label class="control-label " for="state[2]">State</label>
                        <select class="form-control" id="state[2]" name="state[2]">
                            <option selected>State Dropdown</option>
                            @foreach($states as $key=>$state)
                            <option value="{{ $state }}">{{ $state }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-4" style="float: left;">
                        <!-- Company License-->
                        <label for="license_company[2]" class="control-label"> Name on License</label>
                        <input type="text" class="form-control" id="license_company[2]" name="license_company[2]"
                            placeholder="Use Company Name if Applicable">
                    </div>
                    <div class="form-group col-sm-4" style="float: left;">
                        <!-- Licesne Number-->
                        <label class="control-label " for="license_number[2]">License Number</label>
                        <textarea class="form-control" id="license_number[2]" name="license_number[2]" rows="1"></textarea>
                    </div>
                </div>

            </div>
            <i id="add-div" class="fa fa-plus" aria-hidden="true"> Click on plus sign and add additional fields.</i>
            <div class="form-group col-sm-12">
                <!-- Submit button !-->
                <button type="submit" class="btn btn-primary" style="width: 50%;">Submit</button>
            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    jQuery(document).ready(function () {
        var i = 3;
        jQuery("#add-div").click(function () {
            jQuery("#additional-license").append(' <div style="float: left; width: 100%;"> <input type="hidden" name="ordinal_number[' + i + ']" value="' + i + '"> <div class="form-group col-sm-3" style="float: left;"> <label class="control-label " for="state[' + i + ']">State</label> <select class="form-control" id="state[' + i + ']" name="state[' + i + ']"> <option selected>State Dropdown</option> @foreach($states as $key=>$state) <option value="{{ $state }}">{{ $state }}</option> @endforeach </select> </div> <div class="form-group col-sm-4" style="float: left;"> <!-- Company License--> <label for="license_company[' + i + ']" class="control-label"> Name on License</label> <input type="text" class="form-control" id="license_company[' + i + ']" name="license_company[' + i + ']" placeholder="Use Company Name if Applicable"> </div> <div class="form-group col-sm-4" style="float: left;"> <label class="control-label " for="license_number[' + i + ']">License Number</label> <textarea class="form-control" id="license_number[' + i + ']" name="license_number[' + i + ']" rows="1"></textarea> </div> </div>');
            i++;
        });

    });
</script>
@stop
