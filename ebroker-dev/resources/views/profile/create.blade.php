@extends('layout.profile')
@section('content')
<h1>Please select from the following: </h1>
<hr>
<form action="/home/broker/license" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label for="description">
                I am a licensed real estate broker. I am not required to hang
                my license with a supervising broker and I am not currently
                hanging my license with a supervising broker.
        </label><br />
        <label class="radio-inline"><input type="radio" name="available" value="1"
                {{{ (isset($profile->available) && $profile->available == '1') ? "checked" : "" }} }> Yes</label>
        <label class="radio-inline"><input type="radio" name="available" value="0"
                {{{ (isset($profile->available) && $profile->available == '0') ? "checked" : "" }} }> No</label>
    </div>


    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <button type="submit" class="btn btn-primary">Add Product Image</button>
</form>
@endsection
