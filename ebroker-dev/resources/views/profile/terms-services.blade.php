@extends('layouts.profile')

@section('content')

<div class="card bg-secondary text-white">
    <div class="card-body">
        <h1>Terms of Services</h1>
    </div>
</div>
<div class="card bg-light text-dark">
    <div class="card-body">
        <p style="text-align: center;">Lorem ipsum dolor sit amet, elit amet placerat tellus. Nulla parturient
            vestibulum
            in
            ligula vel, consequat at felis arcu et accusantium vel, potenti porttitor tempor at nam quam, nullam
            curabitur
            commodo et. Tempor morbi integer alias purus, nunc odio, praesent eget, phasellus habitasse class in
            vivamus
            vel,
            amet elit wisi condimentum in magna. Morbi aliquam dis turpis, vitae vestibulum sapien elit sit nullam,
            eget
            faucibus feugiat, justo etiam turpis commodo massa dolor habitasse, sed nec pretium mauris. Turpis in sed
            justo
            tristique, eros magna interdum nibh arcu. Sociis dolor. Est faucibus lobortis enim turpis lectus, elementum
            massa
            dolor, vitae consectetuer convallis, quis adipiscing lorem volutpat, dolor nunc diam etiam. Quis et. Ut
            duis
            velit
            consectetuer, mauris vitae, accumsan minima nulla. Lobortis laoreet amet, placerat fermentum euismod nunc
            eget
            vestibulum conubia, ligula leo consectetuer nunc integer. Nibh eget, arcu curabitur accumsan pretium mi
            nec.
            Sem at
            sed venenatis lectus, iaculis varius sem dolor et consectetuer.</p>
        <a href="{{ route('verification') }}">Continue & Require Signature</a>
    </div>
</div>

@stop
