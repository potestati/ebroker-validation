@include('includes/header')

<div class="card">
    <div class="card-header">
        <div class="text-center">
            <h1 class="display-4">
                Home
            </h1>
            <div class="container">
                <p class="lead">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore
                    magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea
                    commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla
                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                    anim id
                    est
                    laborum.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" alt="Bootstrap Thumbnail First" src="{{ URL::to('/') }}/img/real-estate-1.jpg" />
                    <div class="card-block">
                        <h5 class="card-title">
                            Card title
                        </h5>
                        <p class="card-text">
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida
                            at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" alt="Bootstrap Thumbnail Second" src="{{ URL::to('/') }}/img/real-estate-2.jpg" />
                    <div class="card-block">
                        <h5 class="card-title">
                            Card title
                        </h5>
                        <p class="card-text">
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida
                            at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" alt="Bootstrap Thumbnail Third" src="{{ URL::to('/') }}/img/real-estate-3.jpg" />
                    <div class="card-block">
                        <h5 class="card-title">
                            Card title
                        </h5>
                        <p class="card-text">
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida
                            at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<style>
    .card-block {
        padding: 10px;
    }

    .card-img-top {
        width: 100%;
        height: 200px;
    }
</style>
<div class="card-footer">
    @include('includes.footer')
</div>
