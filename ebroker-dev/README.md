# eBrooker

Development for eBrooker. Laravel project.

To-do list:

User levels
- Admin
- Agent

Models
- User
- Profile
- Settings
- Projects


Tables

User

id
name
email
email_verified_at
password
remember_token
created_at
updated_at
admin
verified


Verify

user_id
token
timestamp

Profile

real_estate_broker
real_estate_agent
supervising_broker_account

invite
supervising_broker_name
supervising_broker_email

first_name
last_name
address
city
state
zip_code
phone_number

if(supervisor)
company_website
logo

primary_licence
primary_licence_state
primary_licence_name
primary_licence_id

additional_licence
additional_state
additional_name
additional_id

// tabela za svaku ili sve u jednu pa po id (bolje)

terms_of_services

Project

id
project
team_members ( agenti array )
status ( Active, Queued, Completed )
representation ( Buyer or Seller )
project_state


Property

property
property_ownership
property_address
property_city
property_state
property_zip
peoperty_apns


Others ( Individual who will be signing the documents )

individual_name
individual_relationshop
individual_phone
individual_email
individual_address
individual_city
individual_state
individual_zip

Documents


